package com.natchamon.app0

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    private val TAG = "MainActivity"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val helloButton: Button = findViewById(R.id.hello_button)
        helloButton.setOnClickListener {
            val fullName:TextView = findViewById(R.id.full_name)
            val student_id: TextView = findViewById(R.id.student_id)
            Log.d(TAG, "Full Name - ${fullName.text} Student Id - ${student_id.text}" )

          val Intent  =  Intent(this, HelloActivity::class.java)
            Intent.putExtra("full_name", fullName.text)
            startActivity(Intent)
        }
    }
}