package com.natchamon.app0

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class HelloActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hello)
        val name: TextView = findViewById(R.id.name)
        val full_name: String = intent.getStringExtra("full_name").toString()
        name.text = full_name
    }
}